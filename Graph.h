#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class PairHasher
{
public:
	long long operator()(const pair<int, int> &p) const
	{
		return p.first * 100000ll + p.second;
	}
};

struct Edge
{
	int from;
	int to;
	int next;
	int flow;
	int cap;

	Edge() {}

	Edge(int from, int to, int next, int f, int c) : from(from), to(to), next(next), flow(f), cap(c)
	{}
};

class Graph
{
public:
	enum Type { ADJ_MATRIX, ADJ_LIST, EDGES_LIST };

	Graph()
	{
		numOfVertices = 0;
		type = ADJ_LIST;
		isOriented = isWeighted = false;
	}

	Graph(int n, Type type = ADJ_LIST, bool isOriented = false, bool isWeighted = false)
	{
		this->numOfVertices = n;
		this->type = type;
		this->isOriented = isOriented;
		this->isWeighted = isWeighted;
		switch(this->type)
		{
		case ADJ_MATRIX:
			adjMatrix.assign(n + 1, vector<int>(n + 1));
			break;
		case ADJ_LIST:
			adjList.assign(n + 1, unordered_map<int, int>());
			break;
		case EDGES_LIST:
			break;
		}
	}

	void readGraph(string fileName);

	void writeGraph(string fileName);

	void transformToAdjMatrix();

	void transformToAdjList();

	void transformToEdgesList();

	void addEdge(int from, int to, int weight = 1);

	void removeEdge(int from, int to);

	int changeEdge(int from, int to, int newWeight);

	Graph getSpanningTreePrima();

	Graph getSpanningTreeKruskal();

	Graph getSpanningTreeBoruvka();

	int checkEuler(bool &circleExist);

	int countEdges();

	vector<int> getEuleranTourFleri();
	
	vector<int> getEuleranTourEffective();

	bool dfsWithMarks(int u, int part, vector<int>& marks);

	bool checkBipart(vector<int> &marks);

	bool try_kuhn(int u, vector<bool> &used, vector<int> &mt);

	vector<pair<int, int> > getMaximumMatchingBipart();

	int dfsPush(int u, int sink, int flow, vector<Edge> &edges, vector<int> &head, vector<bool> &used) const;

	Graph flowFordFulkerson(int source, int sink);

	Graph flowDinic(int source, int sink);

	int getNumOfVertices() const;

private:
	Type type;
	int numOfVertices;
	bool isOriented;
	bool isWeighted;
	vector<vector<int> > adjMatrix;
	vector < unordered_map<int, int> > adjList;
	unordered_map<pair<int, int>, int, PairHasher> edgesList;

	int countNonEmptyComponents();
	bool existWay(int u, int destination, pair<int, int> lockedEdge, vector<bool> &used);
	Graph copy();

	
};

#endif // GRAPH_H