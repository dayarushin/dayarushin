#include "Graph.h"
#include "Dsu.h"
#include <string>
#include <fstream>
#include <sstream>
#include <set>
#include <algorithm>
#include <stack>

void Graph::readGraph(string fileName)
{
	ifstream inf;
	inf.open(fileName, ifstream::in);
	char c;
	inf >> c;
	switch (c)
	{
	case 'C':
		this->type = ADJ_MATRIX;
		inf >> this->numOfVertices >> this->isOriented >> this->isWeighted;
		this->adjMatrix.assign(this->numOfVertices + 1, vector<int>(this->numOfVertices + 1, 0));
		for (int i = 1; i <= this->numOfVertices; ++i)
			for (int j = 1; j <= this->numOfVertices; ++j)
				inf >> this->adjMatrix[i][j];
		break;
	case 'L':
		this->type = ADJ_LIST;
		inf >> this->numOfVertices >> this->isOriented >> this->isWeighted;
		this->adjList.resize(this->numOfVertices + 1);
		inf.ignore();
		for (int i = 1; i <= this->numOfVertices; ++i)
		{
			string line;
			getline(inf, line);
			stringstream ss(line);
			int b, w;
			if (this->isWeighted)
				while (ss >> b >> w)
					this->adjList[i][b] = w;
			else while (ss >> b)
				this->adjList[i][b] = 1;
		}
		break;
	case 'E':
		this->type = EDGES_LIST;
		int numOfEdges;
		inf >> this->numOfVertices >> numOfEdges >> this->isOriented >> this->isWeighted;
		for (int i = 0; i < numOfEdges; ++i)
		{
			int from, to, w = 1;
			inf >> from >> to;
			if (this->isWeighted)
				inf >> w;
			this->edgesList[make_pair(from, to)] = w;
		}
		break;
	}
	inf.close();
}

void Graph::writeGraph(string fileName)
{
	ofstream ouf(fileName, ofstream::out);
	switch (type)
	{
	case ADJ_MATRIX:
		ouf << "C " << numOfVertices << endl;
		ouf << (isOriented ? 1 : 0) << ' ' << (isWeighted ? 1 : 0) << endl;
		for (int i = 1; i <= numOfVertices; ++i)
		{
			for (int j = 1; j <= numOfVertices; ++j)
				ouf << adjMatrix[i][j] << ' ';
			ouf << endl;
		}
		break;
	case ADJ_LIST:
		ouf << "L " << numOfVertices << endl;
		ouf << (isOriented ? 1 : 0) << ' ' << (isWeighted ? 1 : 0) << endl;
		for (int i = 1; i <= numOfVertices; ++i)
		{
			for (auto p : adjList[i])
			{
				ouf << p.first << ' ';
				if (isWeighted)
					ouf << p.second << ' ';
			}
			ouf << endl;
		}
		break;
	case EDGES_LIST:
		ouf << "E " << numOfVertices << ' ' << edgesList.size() << endl;
		ouf << (isOriented ? 1 : 0) << ' ' << (isWeighted ? 1 : 0) << endl;
		for (auto edge : edgesList)
		{
			ouf << edge.first.first << ' ' << edge.first.second;
			if (isWeighted)
				ouf << ' ' << edge.second;
			ouf << endl;
		}
		break;
	}
	ouf.close();
}

void Graph::transformToAdjMatrix()
{
	switch (type)
	{
	case ADJ_LIST:
		adjMatrix.assign(numOfVertices + 1, vector<int>(numOfVertices + 1, 0));
		for (int i = 1; i <= numOfVertices; ++i)
			for (auto p : adjList[i])
				adjMatrix[i][p.first] = p.second;
		adjList.clear();
		break;
	case EDGES_LIST:
		adjMatrix.assign(numOfVertices + 1, vector<int>(numOfVertices + 1, 0));
		for (auto edge : edgesList)
		{
			adjMatrix[edge.first.first][edge.first.second] = edge.second;
			if (!isOriented)
				adjMatrix[edge.first.second][edge.first.first] = edge.second;
		}
		edgesList.clear();
		break;
	}
	type = ADJ_MATRIX;
}

void Graph::transformToAdjList()
{
	switch (type)
	{
	case ADJ_MATRIX:
		adjList.assign(numOfVertices + 1, unordered_map<int, int>());
		for (int i = 1; i <= numOfVertices; ++i)
			for (int j = 1; j <= numOfVertices; ++j)
				if (adjMatrix[i][j])
					adjList[i][j] = adjMatrix[i][j];
		adjMatrix.clear();
		break;
	case EDGES_LIST:
		adjList.assign(numOfVertices + 1, unordered_map<int, int>());
		for (auto edge : edgesList)
		{
			adjList[edge.first.first][edge.first.second] = edge.second;
			if (!isOriented)
				adjList[edge.first.second][edge.first.first] = edge.second;
		}
		break;
	}
	type = ADJ_LIST;
}

void Graph::transformToEdgesList()
{
	switch (type)
	{
	case ADJ_MATRIX:
		for (int i = 1; i <= numOfVertices; ++i)
			for (int j = 1; j <= numOfVertices; ++j)
				if (adjMatrix[i][j] && (i <= j || isOriented))
					edgesList[make_pair(i, j)] = adjMatrix[i][j];
		adjMatrix.clear();
		break;
	case ADJ_LIST:
		for (int i = 1; i <= numOfVertices; ++i)
			for (auto p : adjList[i])
				if (i <= p.first || isOriented)
					edgesList[make_pair(i, p.first)] = p.second;
		adjList.clear();
		break;
	}
	type = EDGES_LIST;

}

void Graph::addEdge(int from, int to, int weight)
{
	switch (type)
	{
	case ADJ_MATRIX:
		adjMatrix[from][to] = weight;
		if (!isOriented)
			adjMatrix[to][from] = weight;
		break;
	case ADJ_LIST:
		adjList[from][to] = weight;
		if (!isOriented)
			adjList[to][from] = weight;
		break;
	case EDGES_LIST:
		edgesList[make_pair(from, to)] = weight;
		break;
	}
}

void Graph::removeEdge(int from, int to)
{
	switch (type)
	{
	case ADJ_MATRIX:
		adjMatrix[from][to] = 0;
		if (!isOriented)
			adjMatrix[to][from] = 0;
		break;
	case ADJ_LIST:
		adjList[from].erase(to);
		if (!isOriented)
			adjList[to].erase(from);
		break;
	case EDGES_LIST:
		edgesList.erase(make_pair(from, to));
		if(!isOriented)
			edgesList.erase(make_pair(to, from));
		break;
	}
}

int Graph::changeEdge(int from, int to, int newWeight)
{
	int oldWeight = 0;
	switch (type)
	{
	case ADJ_MATRIX:
		oldWeight = adjMatrix[from][to];
		adjMatrix[from][to] = newWeight;
		if (!isOriented)
			adjMatrix[to][from] = newWeight;
		break;
	case ADJ_LIST:
		oldWeight = adjList[from][to];
		adjList[from][to] = newWeight;
		if (!isOriented)
			adjList[to][from] = newWeight;
		break;
	case EDGES_LIST:
		if(isOriented)
		{
			oldWeight = edgesList[make_pair(from, to)];
			edgesList[make_pair(from, to)] = newWeight;
		}
		else
		{
			if(edgesList.find(make_pair(from, to))!=edgesList.end())
			{
				oldWeight = edgesList[make_pair(from, to)];
				edgesList[make_pair(from, to)] = newWeight;
			}
			else
			{
				oldWeight = edgesList[make_pair(to, from)];
				edgesList[make_pair(to, from)] = newWeight;
			}
		}
		break;
	}
	return oldWeight;
}

Graph Graph::getSpanningTreePrima()
{
	transformToAdjList();
	vector<int> dist(numOfVertices + 1, 1e9);
	vector<int> from(numOfVertices + 1, -1);
	vector<int> used(numOfVertices + 1, false);
	// �������� ������� ������ � ������ �������
	dist[1] = 0;
	set<pair<int, int> > q; // ���������� set ��� ������������ ������� (dist[u], u)
	q.insert(make_pair(0, 1)); // ���������� ������ ������� � ����������� 0
	while (q.size() > 0)
	{
		int u = q.begin()->second;
		used[u] = true;
		q.erase(q.begin());
		for(auto p : adjList[u]) // �������� ����������
		{
			if (!used[p.first] && p.second < dist[p.first])
			{
				q.erase(make_pair(dist[p.first], p.first));
				dist[p.first] = p.second;
				from[p.first] = u;
				q.insert(make_pair(dist[p.first], p.first));
			}
		}
	}
	// �������� �����
	Graph spanTree(numOfVertices, ADJ_LIST, false, isWeighted);
	for (int i = 2; i <= numOfVertices; ++i)
		spanTree.addEdge(from[i], i, adjList[from[i]][i]);
	return spanTree;
}

Graph Graph::getSpanningTreeKruskal()
{
	Dsu dsu(numOfVertices + 1);
	transformToEdgesList();
	vector<pair<int, pair<int, int> > > edges; // (weight, (from, to))
	for(auto edge : edgesList)
		edges.push_back(make_pair(edge.second, edge.first));
	sort(edges.begin(), edges.end());
	Graph spanTree(numOfVertices, ADJ_LIST, false, true);
	for(auto e : edges)
	{
		int u = e.second.first;
		int v = e.second.second;
		if (dsu.find(u) != dsu.find(v)) // ���� � ������ �����������, �� ����������
		{
			dsu.unite(u, v);
			spanTree.addEdge(e.second.first, e.second.second, e.first);
		}
	}
	return spanTree;

}

Graph Graph::getSpanningTreeBoruvka()
{
	transformToAdjList();
	Dsu dsu(numOfVertices + 1);
	Graph spanTree(numOfVertices, EDGES_LIST, false, true);
	while (spanTree.edgesList.size() < numOfVertices - 1)
	{
		// ������ ���. ����� ��� ������ ����������
		// root ���������� - (���, (from, to))
		unordered_map<int, pair<int, pair<int, int> > > minEdge;
		// ������� ��� �����
		for (int i = 1; i <= numOfVertices; ++i)
			minEdge[dsu.find(i)] = make_pair(1e9, make_pair(-1, -1));
		// ���� ����������� �����
		for (int i = 1; i <= numOfVertices; ++i)
		{
			int root = dsu.find(i);
			for(auto e : adjList[i])
				if (dsu.find(e.first) != root)
				{
					minEdge[root] = min(minEdge[root], make_pair(e.second, make_pair(i, e.first)));
				}
		}
		// ��������� ��������� ����������� �����
		for each(pair<int, pair<int, pair<int, int> > > e in minEdge)
		{
			int weight = e.second.first;
			int from = e.second.second.first;
			int to = e.second.second.second;
			if (spanTree.edgesList.find(make_pair(to, from)) == spanTree.edgesList.end()) // ������ �� ���������� ����������
			{
				spanTree.addEdge(from, to, weight);
				dsu.unite(from, to);
			}
		}
	}
	return spanTree;
}

int Graph::countNonEmptyComponents()
{
	transformToEdgesList();
	Dsu dsu(numOfVertices + 1);
	for(auto e : edgesList)
		dsu.unite(e.first.first, e.first.second);
	set<int> roots;
	for (int i = 1; i <= numOfVertices; ++i)
		if (dsu.find(i) != i) // ���� �������� ������� �� ����� ����� �������, ������ � ���������� ���� �����
			roots.insert(dsu.find(i));
	return roots.size();
}

int Graph::checkEuler(bool& circleExist)
{
	if (countNonEmptyComponents()>1)
	{
		circleExist = false;
		return 0;
	}
	transformToAdjList();
	int oddCount = 0;
	for (int i = 1; i <= numOfVertices; ++i)
		if (adjList[i].size() % 2 == 1)
			oddCount++;
	if (oddCount == 0)
	{
		circleExist = true;
		return 1; // ����� �������� � �����, ��� ��� ���� ����
	}
	circleExist = false;
	if (oddCount == 2)
	{
		for (int i = 1; i <= numOfVertices; ++i)
			if (adjList[i].size() % 2 == 1)
				return i;
	}
	else return 0;
}

int Graph::countEdges()
{
	transformToEdgesList();
	return edgesList.size();
}

// ���� ���� � destination, �� ��������� lockedEdge. ��� ����� ����������, �������� �� ������ lockedEdge
bool Graph::existWay(int u, int destination, pair<int, int> lockedEdge, vector<bool> &used)
{
	if (u == destination)
		return true;
	used[u] = true;
	for(auto e : adjList[u])
		if (!used[e.first] && make_pair(u, e.first) != lockedEdge)
			if (existWay(e.first, destination, lockedEdge, used))
				return true;
	return false;
}

// ������ ����������� �����
Graph Graph::copy()
{
	transformToEdgesList();
	Graph graphCopy(numOfVertices, EDGES_LIST, isOriented, isWeighted);
	graphCopy.edgesList = edgesList;
	return graphCopy;
}

vector<int> Graph::getEuleranTourFleri()
{
	int numOfEdges = countEdges();
	transformToAdjList();
	auto graphCopy = this->copy(); // ��������� ����, ����� ����� ���� �������� ������� ����
	bool circleExist;
	int start = checkEuler(circleExist);
	vector<int> tour;
	tour.push_back(start);
	if (start)
	{
		int u = start;
		for (int j = 0; j < numOfEdges; ++j)
		{
			vector<int> bridges, non_bridges;
			// ������ ����� � �� ����� �� ������� �������
			for (auto e : adjList[u])
			{
				vector<bool> used(numOfVertices + 1, false);
				if (existWay(u, e.first, make_pair(u, e.first), used)) 
					non_bridges.push_back(e.first);
				else bridges.push_back(e.first);
			}
			// ���� ���� ������, �� ����� ���
			if (non_bridges.size()>0) 
			{
				tour.push_back(non_bridges[0]);
				removeEdge(u, non_bridges[0]);
				u = non_bridges[0];
			}
			else // � ������� ������ ������� ��� �� ����
			{
				tour.push_back(bridges[0]);
				removeEdge(u, bridges[0]);
				u = bridges[0];
			}
		}
	}
	*this = graphCopy.copy(); // ����������� ��� ����
	return tour;

}

vector<int> Graph::getEuleranTourEffective()
{
	transformToAdjList();
	bool circleExist;
	int start = checkEuler(circleExist);
	vector<int> tour;
	if (start)
	{
		unordered_map<pair<int, int>, bool, PairHasher> used;
		stack<int> st;
		st.push(start);
		while (!st.empty())
		{
			int u = st.top();
			// ���� ����� �����, �� �������� ��� �� ������
			for (auto e : adjList[u])
			{
				int v = e.first;
				if (!used[make_pair(u, v)] && !used[make_pair(v, u)]) 
				{
					st.push(v);
					used[make_pair(u, v)] = true;
					break;
				}
			}
			if (u == st.top()) // �� ������� �����, �� ������� ��� �� ��������
			{
				st.pop();
				tour.push_back(u);
			}
		}
	}
	return tour;
}

// ��������� ���������� ���� �� ���������� � ���������, ��� ��� ������� ����� �������
bool Graph::dfsWithMarks(int u, int part, vector<int>& marks)
{
	if (marks[u] != -1)
		return marks[u] == part;
	marks[u] = part;
	bool isBipart = true;
	for (auto e : adjList[u])
		isBipart &= dfsWithMarks(e.first, 1 - part, marks);
	return isBipart;
}

bool Graph::checkBipart(vector<int>& marks)
{
	transformToAdjList();
	marks.assign(numOfVertices + 1, -1); // -1 - �� ��������, 0 - ������ ����, 1 - 2 ����
	bool isBipart = true;
	for (int i = 1; i <= numOfVertices; ++i)
		if (marks[i] == -1)
			isBipart &= dfsWithMarks(i, 0, marks);
	return isBipart;
}

bool Graph::try_kuhn(int u, vector<bool>& used, vector<int>& mt)
{
	if (used[u]) return false;
	used[u] = true;
	for(auto e : adjList[u])
	{
		int to = e.first;
		if(mt[to]==-1 || try_kuhn(mt[to], used, mt))
		{
			mt[to] = u;
			return true;
		}
	}
	return false;
}

vector<pair<int, int>> Graph::getMaximumMatchingBipart()
{
	vector<pair<int, int> > matching;
	vector<int> marks(numOfVertices + 1, -1); // -1 - �� ��������, 0 - ������ ����, 1 - 2 ����
	vector<bool> used(numOfVertices + 1, false); // ����� �������������� ������ ��� ������ ������ ����
	vector<int> mt(numOfVertices + 1, -1); // ����� �������������� ������ ��� ������ ������ ����, ������ ���� �� ������
	if(checkBipart(marks))
	{
		transformToAdjList();
		// ������ � ������ ��� ���������
		vector<bool> preused(numOfVertices + 1, false);
		for (int i = 1; i <= numOfVertices; ++i)
			if(marks[i]==0)
				for(auto e : adjList[i])
					if(mt[e.first]==-1)
					{
						mt[e.first] = i;
						preused[i] = true;
						break;
					}

		for (int i = 1; i <= numOfVertices; ++i) 
			if(marks[i]==0 && !preused[i]) // �� ������ ������� ������ ���� �������� ����� ������������� ����
			{
				used.assign(numOfVertices + 1, false);
				try_kuhn(i, used, mt);
			}
		for (int i = 1; i <= numOfVertices; ++i)
			if (mt[i] != -1)
				matching.push_back(make_pair(mt[i], i));
	}
	return matching;
}

int Graph::dfsPush(int u, int sink, int flow, vector<Edge>& edges, vector<int>& head, vector<bool>& used) const
{
	if (u == sink) return flow;
	used[u] = true;
	for (int i = head[u]; i != -1; i=edges[i].next)
		if(!used[edges[i].to] && edges[i].flow<edges[i].cap)
		{
			int pushed = dfsPush(edges[i].to, sink, min(flow, edges[i].cap - edges[i].flow), edges, head, used);
			if(pushed)
			{
				edges[i].flow += pushed;
				edges[i ^ 1].flow -= pushed;
				return pushed;
			}
		}
	return 0;
}

Graph Graph::flowFordFulkerson(int source, int sink)
{
	transformToEdgesList();
	vector<Edge> edges;
	edges.reserve(edgesList.size() * 2);
	vector<int> head(numOfVertices + 1, -1); // ������ ������ ����� ��� ������ �������
	for(auto e : edgesList)
	{
		edges.push_back(Edge(e.first.first, e.first.second, head[e.first.first], 0, e.second)); // ������ �����
		edges.push_back(Edge(e.first.second, e.first.first, head[e.first.second], 0, 0)); // �������� �����
		head[e.first.first] = edges.size() - 2;
		head[e.first.second] = edges.size() - 1;
	}
	vector<bool> used(numOfVertices + 1, false);
	while (dfsPush(source, sink, 1e9, edges, head, used))
		used.assign(numOfVertices + 1, false);
	Graph flowGraph = this->copy();
	for (int i = 0; i < edges.size(); i += 2)
		flowGraph.edgesList[make_pair(edges[i].from, edges[i].to)] = edges[i].flow;
	return flowGraph;
}

Graph Graph::flowDinic(int source, int sink)
{
	throw "not implemented";
}

int Graph::getNumOfVertices() const
{
	return numOfVertices;
}
