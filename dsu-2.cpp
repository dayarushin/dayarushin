#include "stdafx.h"
#include <string>
#include <vector>
#include "dsu.h"

using namespace std;

DSU::DSU(int N)
{
	for(int i = 0; i < N; i++)
	{
		parent.push_back(i);
	}
}

int DSU::find(int x)
{
	if (x == parent[x])
		return x;
	return find (parent[x]);
}

void DSU::unite(int x, int y)
{
	x = find(x);
	y = find(y);
	if (x != y)
		parent[y] = x;
}

int DSU::count()
{
	return parent.size();
}