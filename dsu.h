#pragma once
#include "stdafx.h"
#include <string>
#include <vector>

using namespace std;

class DSU
{
private:
	vector<int> parent;
public:
	DSU::DSU(int N);
	int DSU::count();
	int DSU::find(int x);
	void DSU::unite(int x, int y);
};